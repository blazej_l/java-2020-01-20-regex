#TASK 1
#The expression is looking for a proper time and date format YYYY-MM-DD HH:MM or YYYY-MM-DD

^[0-9]{4}-[0|1][0-9]-[0-9]{2} [0-2]{1}[0-9]:[0-5][0-9]$|^[0-9]{4}-[0|1][0-9]-[0-9]{2}$

#TASK 2
#The expression is looking for text lines with 'roar'

^.*roar.*$

#TASK 3
#The expression is looking for name and surname patterns

^([A-Z]|[ĄĆĘŁŃÓŚŹŻ])([a-z]|[ąćęłńóśźż])+ ([A-Z]|[ĄĆĘŁŃÓŚŹŻ])([a-z]|[ąćęłńóśźż])+$
